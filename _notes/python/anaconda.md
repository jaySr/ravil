---
layout: notes
title: Anaconda
aside:
  toc: true
sidebar:
  nav: notes-nav
---

<!-- vim-markdown-toc GitLab -->

* [Environment Management](#environment-management)

<!-- vim-markdown-toc -->

## Environment Management

Updating all environment packages:

```
conda update --all
```

Cloning an environment:

```
conda create -n new-env --clone base
```

Exporting an environment:

```
conda env export | grep -v "^prefix: " > environment.yml
```

Re-creating an environment from a file:

```
conda env create -f environment.yml
```
