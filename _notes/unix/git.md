---
layout: notes
title: Git
aside:
  toc: true
sidebar:
  nav: notes-nav
---

- [Managing remotes](#managing-remotes)
- [Branching](#branching)

## Managing remotes

Add a named remote (you can have multiple remotes):

```
git remote add github https://github.com/user/repo.git

git remote add gitlab https://gitlab.com/user/repo.git

git push github
git push gitlab
```

Overloading origin with another remote:

```
git remote set-url ––add origin https://github.com/user/repo.git
```

Deleting a remote:

```
git remote remove origin
```

## Branching

Deleting a local branch:

```
git branch -d branch_name
```

Deleting a remote branch:

```
push --delete remote_name branch
```

Checkout specific files from another branch:

```
git checkout branch_name file file2
```

Merge a specific commit to current branch:

```
git cherry-pick 63344f2
```

Hide branch changes:

```
git stash
```

Restore branch changes:

```
git stash apply
```
