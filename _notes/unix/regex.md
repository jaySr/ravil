---
layout: notes
title: Regular Expressions
aside:
  toc: true
sidebar:
  nav: notes-nav
---

- [Match Location](#match-location)
- [Datatypes](#datatypes)

## Match Location

| Action      | Regex     |
| :---------- | :-------- |
| Match start | `^string` |
| Match end   | `string$` |

## Datatypes

| Action                  | Regex |
| :---------------------- | :---- |
| Match integer           | `\d`  |
| Match multiple integers | `\d+` |

