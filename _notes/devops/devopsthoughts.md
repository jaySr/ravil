---
layout: notes
title: Thoughts on DevOps
aside:
  toc: true
sidebar:
  nav: notes-nav
---

This is a collection of thoughts on what constitutes good project management and efficient software development practices. Most ideas are shamelessly borrowed from articles, forums, and other people smarter than me.

## Anti-agile

"Agile" is an adjective, not a noun. It should never be capitalized, with exception of when it is in a title.

The key points from the original "Manifesto for Agile Software Development" (2001):

1. **Individuals and interactions** over processes and tools
2. **Working software** over comprehensive documentation
3. **Customer collaboration** over contract negotiation
4. **Responding to change** over following a plan


### People, not processes

The first point of the original manifesto: "Individuals and interactions over processes and tools", is ironically, the most often violated in corporate Agile™ implementations, such as Scrum.

Key takeaways:

> The thing to manage isn’t tasks, requirements, and sprints. The thing to manage is: “Is this team effective, enough to be trusted?”

### On delivering on time

Uncertainty is inherent in the development process. It is called Research and Development.

> To achieve predictability, something has to give. It’s usually “learning” which is never on the plan.

The key is to manage expectations.

> The next release will be done when the right top priorities are met well enough. When’s that? You decide by prioritizing how many priorities are in the release and your bar on quality.

Or as my old boss once told me:

> You have to know how to plan the work, and work the plan.


