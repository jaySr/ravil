---
layout: article
title: Blogs
show_date: false
---

A Curated List of My Favorite Blogs.

## Cheminformatics

[Is life worth living?](https://iwatobipen.wordpress.com/)

[Practical Cheminformatics](http://practicalcheminformatics.blogspot.com/)

## Software Development

[Thomas Kainrad](https://tkainrad.dev/)

## Linux

[GNOME Shell & Mutter](https://blogs.gnome.org/shell-dev/)

## Photography

[Paul Stamatiou](https://paulstamatiou.com/)

