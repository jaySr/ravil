---
layout: article
title: About
excerpt: Ricardo Avila is a bioinformatics nerd, artist, and lover of open source software.
---

Hi! 👋 My name is Ricardo Avila. I enjoy researching computational problems in biology, ranging from protein structures, to genomic sequences and chemical structures. I find that the most difficult and rewarding problems often require connecting seemingly unrelated fields. Hence, I enjoy being in the overlap of different disciplines, figuring out how things come together.

I grew up in the sunny city of El Paso, Texas, and graduated from the University of Texas at El Paso (UTEP), where I studied a Bachelor's of Science in Biochemistry, and a Master's degree in Bioinformatics. My current research involves machine learning methods for drug discovery, but I'm also very interested in protein structure and molecular simulation.

In my free time I like drawing, taking photographs, reading science fiction, and playing music. For more of that, you can visit my second homepage:
[ravilart.com](https://ravilart.com){:.button.button--primary.button--pill}

## About this site

This blog has seen several iterations in my attempts at finding the best format for writing. I needed something that can easily display code, documentation and mathematical formulas. Therefore, I have settled on writing these posts in markdown using Jekyll, and hosting the source code on GitLab.


In this small blog, I plan to make regular posts about bioinformatics projects, and miscellaneous code snippets. Most of the time I am coding in Python, R, or BASH... but I will post about whatever I am learning at the moment. My main motivation is that I frequently find myself trying to remember the solution to an old problem that I know I solved before, but spending a long time trying to remember which project it came up in. Hence, I have decided that blogging would be a good habit to build. I am hoping that some of these posts can be helpful to other people as well.

## Contact

I'm friendly! If you have any questions or comments, feel free to shoot me an email.

📧 ravila@protonmail.com

<div class="card">
  <div class="card__image">
    <img class="image" src="/assets/author.jpg"/>
  </div>
 <div class="card__content">
    <div class="card__header">
      <h4>The Author</h4>
    </div>
    <p>Ricardo Avila is a bioinformatics nerd, artist, and lover of open source software.</p>
  </div>
</div>
