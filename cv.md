---
layout: article
title: Curriculum Vitae
aside:
  toc: true
---

__Last Updated__: 02/20/2020.

## Education

- `2019` __M.S. Bioinformatics__, University of Texas at El Paso (UTEP)
- `2016` __B.S. Biochemistry__, University of Texas at El Paso (UTEP)

## Appointments

### GlaxoSmithKline
- `08/2019 - 02/2020` Co-op, Medical Insights: Data Analysis and Visualization / DevOps Engineering
- `05/2019 - 08/2019` Intern, Protein Design and Informatics
- `05/2018 - 08/2018` Intern, Protein Design and Informatics

### University of Texas at El Paso
- `01/2019 - 05/2019` Teaching Assistant, [UTEP Bioinformatics Program](http://www.bioinformatics.utep.edu/home.php)
- `01/2018 - 05/2019` Graduate Research Assistant, [Dr. Suman Sirimulla](http://www.sirimullaresearchgroup.com/), UTEP School of Pharmacy
- `08/2016 - 08/2017` Graduate Research Assistant, [Dr. Ming-Ying Leung](http://www.math.utep.edu/Faculty/mleung/home.php), UTEP Bioinformatics Program
- `03/2014 - 08/2015` Undergraduate Research Assistant, [Dr. Ricardo Bernal](https://science.utep.edu/chemistry/rbernal/), UTEP Department of Chemistry

## Presentations

### Posters
- `2018` Evaluating the effectiveness of mixed probe molecular dynamics for cryptic pocket detection. GSK Intern/Co-op symposium, Upper Providence, Pennsylvania, USA.
- `2018` Automated workflow for reproducible analysis of protein-ligand scoring functions. [OpenEye CUP XVIII](https://www.eyesopen.com/events/2018/cup-18), Santa Fe, New Mexico, USA.
- `2017` In silico evaluation of human OX2 orexin receptor (hOX2R) inhibitors using AutoDock and Glide. [12th Annual NMBIST Symposium on Microbiomics](http://www.cvent.com/events/12th-annual-nmbist-symposium-on-microbiomics/event-summary-8c84879ef6c34e69ad99bde87b91038b.aspx?dvce=1), Santa Fe, New Mexico, USA.
- `2016` Comparison of AutoDock and Glide in docking Suvorexant to the crystal structure of hu-
man OX2 receptor (hOX2R) [19th UTEP/NMSU Workshop on Mathematics, Computer Science, and Computational Sciences](http://www.cs.utep.edu/vladik/utepnmsu16.html), El Paso, Texas, USA.
- `2015` A point mutation in the Phi-EL chaperonin alters ring separation and protein folding activity. UTEP COURI Symposium, El Paso, Texas, USA.

## Workshops

- `May 7-9, 2018` [MMTF Workshop & Hackathon](https://www.eiseverywhere.com/ehome/index.php?eventid=319393&), San Diego Supercomputer Center (SDSC), San Diego, CA
- `August 28, 2019` [Dashboard in a Day](https://powerbi.microsoft.com/en-us/diad/), Microsoft, Malvern, PA

## Awards

- `2011` __Presidential Scholarship__, from The University of Texas at El Paso
- `2018` __MMTF Workshop Travel Award__, funded by the NIH Big Data to Knowledge (BD2K) initiative

## Skills

### Programming Languages
- Python
- UNIX shell (BASH)
- R
- MySQL
- Java

### DevOps & Cloud Computing
- Git
- Containerization: Docker, Singularity
- High Performance Computing (HPC)
- Databricks
- Microsoft Azure: Azure DevOps, Azure Data Factory
- GitLab Continuous Integration

### Protein & Molecular Informatics
- Cheminformatic toolkits: RDKit, OpenEye
- Molecular docking: AutoDock Vina, Glide
- Molecular viewers: PyMol, Chimera, NGLView, VMD, Schrödinger Maestro
- Pocket detection: MOE Site Finder, Schrödinger SiteMap

### Structural Biology
- Cryo-EM image processing: Relion 3, EMAN2
- Molecular Biology: Protein expression, Protein purification, Crystal screening.
 
