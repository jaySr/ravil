# Ricardo Avila's Bioinformatics Blog

## Getting Started
1. Clone this repository and `cd` into it
2. Install Jekyll and Bundler: `gem install jekyll bundler`
3. Install required packages: `bundle install --path vendor/bundle`
4. Start server: `bundle exec jekyll serve`

## Creating New Blog Posts

All blog post writing is to take place in the "authoring" branch.
The markdown files follow the naming convention: `YYYY_MM_DD_a_good_title.md`, and are placed in the `_posts` folder.
They can be written from scratch, or generated from a Jupyter notebook.

Blog posts also start with the following YAML front matter:

```
---
title: "Post Title"
excerpt: "A short description."
tags:
    - tag1
    - tag2
---
```

The authoring branch contains additional folders not in master:
- **notebooks/** - store original Jupyter notebooks.
- **templates/** - templates for converting Jupyter notebooks to markdown.
- **jekyll.py** - config script for converting Jupyter to markdown.

Current workflow for generating posts from Jupyter Notebooks:

1. Place Jupyter notebooks to convert in the "notebooks" folder of **authoring** branch.
2. From this folder, run:  `jupyter nbconvert --to markdown <notebook>.ipynb --config ../jekyll.py`
3. Copy any images to "assets/images".
4. Edit the markdown file that was generated in "_posts", fixing any broken links and formatting issues.
5. Commit changes when satisfied with the post.
6. When ready to publish, change to the **master** branch and cherrypick the last commit.
7. Push **master** branch to origin.

## Creating New Notes

Notes have the following YAML front matter:

```
---
layout: notes
title: "Title"
aside:
  toc: true
sidebar:
  nav: notes-nav
---
```

After creating a new note page, add it to the "notes-nav" section of `_data/navigation.yml`.

## Continuous Integration
When a commit is made to **master**, the website is built and deployed.
The file **.gitlab-ci.yml** contains the build configuration. 

