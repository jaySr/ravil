---
title: "Machine Learning Methods for LogP Prediction: Pt. 1"
excerpt: "The octanol-water partition coefficient, or logP, is one of the most important properties for determining a compound’s suitability as a drug. Unfortunately, currently existing models are not as accurate as we would like. This experiment compares the performance of several different molecular fingerprinting methods, paired with three common machine learning algorithms."
tags:
    - Python
    - logP
    - machine learning
    - fingerprint calculation
--- 

- [Reading experimetal logP data](#reading-experimetal-logp-data)
- [Model with simple descriptors](#model-with-simple-descriptors)
- [Calculating fingerprints](#calculating-fingerprints)
- [Comparing fingerprint models](#comparing-fingerprint-models)

The octanol-water partition coefficient, or logP, is one of the most
important properties for determining a compound's suitability as a drug.
Currently, most of the available regression models for *in silico* logP
prediction are trained on the [PHYSPROP](https://www.srcinc.com/what-we-
do/environmental/scientific-databases.html) database of experimental logP
values. However most of the compounds in this database are not highly
representative of the drug-like chemical space. Unfortunately, there is
currently a lack of publicly available experimental logP datasets for biological compounds which
can be used to train better prediction tools. 
 
In this small test, I have decided to use the experimental logP data released
in the paper: "Large, chemically diverse dataset of logP measurements for
benchmarking studies" by Martel et al[^1]. As this is a preliminary study, we are interested in finding which featurization methods work best for predicting logP.

[^1]: Martel, S., Gillerat, F., Carosati, E., Maiarelli, D., Tetko, I. V., Mannhold, R., & Carrupt, P.-A. (2013). Large, chemically diverse dataset of logP measurements for benchmarking studies. European Journal of Pharmaceutical Sciences, 48(1-2), 21–29. doi: 10.1016/j.ejps.2012.10.019 

Most of the popular tools for logP prediction are based on 
physical descriptors, such as atom type counts, or polar surface area,
or on topological descriptors. Here, we will calculate different physical
descriptors, as well as structural fingerprints for the molecules, and 
benchmark their performance using three different regression models:
neural network, random forest, and support vector machines. 
 
We first import some libraries including RDKit and scikit-learn tools
(The utility script contains custom functions for generating TPATF and TPAPF
fingerprints):


{% highlight python %}
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import Descriptors

from utility import FeatureGenerator

from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.svm import SVR
{% endhighlight %}

The utility script can be found in this [Gist](https://gist.github.com/ravila4/a18bb9d0e3f54de3c9fb8f75908f992f).

 
## Reading experimetal logP data 
 
The supplementary pdf file from the Martel et al. paper was converted to csv text format using the Linux [pdftotext](https://linux.die.net/man/1/pdftotext)
utility from the Poppler library.
The experimental data is read as a csv file, and the SMILES strings are
converted to RDKit molecules.


{% highlight python %}
data = pd.read_csv("training_data/logp_759_data.csv")
data_logp = data[data.Status == "Validated"]
print("Shape:", data_logp.shape)
data_logp.head()
{% endhighlight %}

`Shape: (707, 7)`


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="0" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ID</th>
      <th>ZINC (2010)</th>
      <th>Status</th>
      <th>Supplier</th>
      <th>SMILES</th>
      <th>logPexp</th>
      <th>pH_of_analysis</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>ZINC00036522</td>
      <td>Validated</td>
      <td>Specs</td>
      <td>Cc1cc2c(cc1C)NC(=O)C[C@H]2c3ccccc3OC</td>
      <td>4.17</td>
      <td>5.0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>3</td>
      <td>ZINC00185379</td>
      <td>Validated</td>
      <td>ChemBridge</td>
      <td>COc1ccc2c(c1)O[C@@](CC2=O)(C(F)(F)F)O</td>
      <td>2.79</td>
      <td>5.0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4</td>
      <td>ZINC12402487</td>
      <td>Validated</td>
      <td>ChemBridge</td>
      <td>CC1(O[C@H]([C@H](O1)C(=O)N)C(=O)N)C(C)(C)C</td>
      <td>1.60</td>
      <td>6.5</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5</td>
      <td>ZINC00055459</td>
      <td>Validated</td>
      <td>Specs</td>
      <td>CCOc1cc(cc(c1OCC)OCC)c2nnc(o2)c3ccco3</td>
      <td>3.96</td>
      <td>10.5</td>
    </tr>
    <tr>
      <th>4</th>
      <td>6</td>
      <td>ZINC00056871</td>
      <td>Validated</td>
      <td>Enamine</td>
      <td>CN(C)c1ccc(cc1)C(=C)c2ccc(cc2)N(C)C</td>
      <td>5.30</td>
      <td>7.3</td>
    </tr>
  </tbody>
</table>
</div>


 
Convert SMILES to 2D molecules: 


{% highlight python %}
molecules = data_logp.SMILES.apply(Chem.MolFromSmiles)
{% endhighlight %}
 
Next, we use RDKit to calculate some physical descriptors: 


{% highlight python %}
data_logp.loc[:, 'MolLogP'] = molecules.apply(Descriptors.MolLogP)
data_logp.loc[:, 'HeavyAtomCount'] = molecules.apply(Descriptors.HeavyAtomCount)
data_logp.loc[:, 'HAccept'] = molecules.apply(Descriptors.NumHAcceptors)
data_logp.loc[:, 'Heteroatoms'] = molecules.apply(Descriptors.NumHeteroatoms)
data_logp.loc[:, 'HDonor'] = molecules.apply(Descriptors.NumHDonors)
data_logp.loc[:, 'MolWt'] = molecules.apply(Descriptors.MolWt)
data_logp.loc[:, 'RotableBonds'] = molecules.apply(Descriptors.NumRotatableBonds)
data_logp.loc[:, 'RingCount'] = molecules.apply(Descriptors.RingCount)
data_logp.loc[:, 'Ipc'] = molecules.apply(Descriptors.Ipc)
data_logp.loc[:, 'HallKierAlpha'] = molecules.apply(Descriptors.HallKierAlpha)
data_logp.loc[:, 'NumValenceElectrons'] = molecules.apply(Descriptors.NumValenceElectrons)
data_logp.loc[:, 'SaturatedRings'] = molecules.apply(Descriptors.NumSaturatedRings)
data_logp.loc[:, 'AliphaticRings'] = molecules.apply(Descriptors.NumAliphaticRings)
data_logp.loc[:, 'AromaticRings'] = molecules.apply(Descriptors.NumAromaticRings)
{% endhighlight %}

 
As a baseline, we calculate the performance of RDKit's calculated MolLogP vs
the experimental logP. 


{% highlight python %}
r2 = r2_score(data_logp.logPexp, data_logp.MolLogP)
mse = mean_squared_error(data_logp.logPexp, data_logp.MolLogP)
plt.scatter(data_logp.logPexp, data_logp.MolLogP,
            label = "MSE: {:.2f}\nR^2: {:.2f}".format(mse, r2))
plt.legend()
plt.show()
{% endhighlight %}

 
![svg]({{ BASE_PATH }}/assets/images/2019-3-7-machine-learning-methods-for-log-p-prediction-1_13_0.svg) 

 
As we can see above, RDKit's logP predictions have a relatively high mean
square error, and a weak coefficient of determination for this dataset. RDKit's MolLogP implementation is based on atomic contributions. Hence, we will first
try to train our own simple logP model using the RDKit physical descriptors
that we generated above.
 
## Model with simple descriptors
 
These are the descriptors that we will use for the model: 


{% highlight python %}
X = data_logp.iloc[:, 8:]
y = data_logp.logPexp
X.head()
{% endhighlight %}




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: left;
    }
</style>
<table border="0" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>HeavyAtomCount</th>
      <th>HAccept</th>
      <th>Heteroatoms</th>
      <th>HDonor</th>
      <th>MolWt</th>
      <th>RotableBonds</th>
      <th>RingCount</th>
      <th>Ipc</th>
      <th>HallKierAlpha</th>
      <th>NumValenceElectrons</th>
      <th>SaturatedRings</th>
      <th>AliphaticRings</th>
      <th>AromaticRings</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>21</td>
      <td>2</td>
      <td>3</td>
      <td>1</td>
      <td>281.355</td>
      <td>2</td>
      <td>3</td>
      <td>69759.740168</td>
      <td>-2.29</td>
      <td>108</td>
      <td>0</td>
      <td>1</td>
      <td>2</td>
    </tr>
    <tr>
      <th>1</th>
      <td>18</td>
      <td>4</td>
      <td>7</td>
      <td>1</td>
      <td>262.183</td>
      <td>1</td>
      <td>2</td>
      <td>7977.096898</td>
      <td>-1.76</td>
      <td>98</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <th>2</th>
      <td>16</td>
      <td>4</td>
      <td>6</td>
      <td>2</td>
      <td>230.264</td>
      <td>2</td>
      <td>1</td>
      <td>2165.098769</td>
      <td>-1.14</td>
      <td>92</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>25</td>
      <td>7</td>
      <td>7</td>
      <td>0</td>
      <td>344.367</td>
      <td>8</td>
      <td>3</td>
      <td>819166.201010</td>
      <td>-2.96</td>
      <td>132</td>
      <td>0</td>
      <td>0</td>
      <td>3</td>
    </tr>
    <tr>
      <th>4</th>
      <td>20</td>
      <td>2</td>
      <td>2</td>
      <td>0</td>
      <td>266.388</td>
      <td>4</td>
      <td>2</td>
      <td>32168.378171</td>
      <td>-2.22</td>
      <td>104</td>
      <td>0</td>
      <td>0</td>
      <td>2</td>
    </tr>
  </tbody>
</table>
</div>


 
For the regression, we will use a Random Forest with the default
parameters from scikit-learn, and set aside one third of the data for testing.


{% highlight python %}
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

models = {"rf": RandomForestRegressor(n_estimators=100, random_state=42)}

scores = {}
for m in models:
    models[m].fit(X_train, y_train)
    scores[m + "_train"] = models[m].score(X_train, y_train, )
    y_pred = models[m].predict(X_test)
    scores[m + "_test"] = r2_score(y_test, y_pred)
    scores[m + "_mse_test"] = mean_squared_error(y_test, y_pred)
{% endhighlight %}
 
The scores of our model are: 


{% highlight python %}
scores = pd.Series(scores).T
scores
{% endhighlight %}

`rf_train       0.909276`  
`rf_test        0.451319`  
`rf_mse_test    0.792195`  
`dtype: float64`




{% highlight python %}
r2 = r2_score(y_test, y_pred)
mse = mean_squared_error(y_test, y_pred)
plt.scatter(y_test, y_pred, label = "MSE: {:.2f}\nR^2: {:.2f}".format(mse, r2))
plt.legend()
plt.show()
{% endhighlight %}

 
![svg]({{ BASE_PATH }}/assets/images/2019-3-7-machine-learning-methods-for-log-p-prediction-1_22_0.svg) 

 
As we can see, using these simple descriptors coupled with scikit-learn's
default random forest gets us a higher R<sup>2</sup> and MSE performance than the RDKit
logP predictor. This, however is likely due to the differences in the
training set that we used, versus the one that they used to develop their model.
It would be interesting to see how much we can improve the performance by tuning
the random forest parameters, and then measure the performance on the PHYSPROP dataset. 
 
## Calculating fingerprints 
 
Now that we saw the performace of the simple molecular descriptors, we would
like to assess the performance of some of the most popular molecular
fingerprints. Among the many available  methods, we will test Morgan
fingerprints (ECFP4 and ECFP6), RDKFingerprints, and topological pharmacophore
fingerprints (TPAPF and TPATF), the scripts for which are available from
[MayaChemTools](http://www.mayachemtools.org/). 
 
I created a function for parallelizing a DataFrame's apply() function. This
makes TPATF and TPAPF fingerprint calculation much faster. This function has
become one of my most useful snippets of code: 


{% highlight python %}
import multiprocessing
from joblib import Parallel, delayed

def applyParallel(df, func):
    """This function splits a pandas Series into n chunks,
    corresponding to the number of available CPUs. Then it
    applies a given function to the dataframe chunks, and 
    finally, returns their concatenated output."""
    n_jobs=multiprocessing.cpu_count()
    groups =  np.array_split(df, n_jobs)
    results = Parallel(n_jobs)(delayed(lambda g: g.apply(func))(group) for group in groups)
    return pd.concat(results)
{% endhighlight %}
 
Calculate fingerprints: 


{% highlight python %}
fps = {"ECFP4": molecules.apply(lambda m: AllChem.GetMorganFingerprintAsBitVect(m, radius=2, nBits=2048)),
       "ECFP6": molecules.apply(lambda m: AllChem.GetMorganFingerprintAsBitVect(m, radius=3, nBits=2048)),
       "RDKFP": molecules.apply(lambda m: AllChem.RDKFingerprint(m, fpSize=2048)),
       "TPATF": applyParallel(data_logp.SMILES, lambda m: FeatureGenerator(m).toTPATF()),
       "TPAPF": applyParallel(data_logp.SMILES, lambda m: FeatureGenerator(m).toTPAPF())}
{% endhighlight %}
 
## Comparing fingerprint models
 
Finally, here we apply three different types of regression models to estimate
the performance of the different fingerprints. 


{% highlight python %}
y = data_logp.logPexp

models = {"rf": RandomForestRegressor(n_estimators=100, random_state=42),
          "nnet": MLPRegressor(random_state=42),
          "svr": SVR(gamma='auto')}

scores = {}

for f in fps:
    scores[f] = {}
    # Convert fps to 2D numpy array
    X = np.array(fps[f].tolist())
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33,
                                                        random_state=42)
    for m in models:
        models[m].fit(X_train, y_train)
        #scores[f][m + "_r2_train"] = models[m].score(X_train, y_train)
        y_pred = models[m].predict(X_test)
        scores[f][m + "_r2_test"] = r2_score(y_test, y_pred)
        scores[f][m + "_mse_test"] = mean_squared_error(y_test, y_pred)
{% endhighlight %}



{% highlight python %}
scores_df = pd.DataFrame(scores).T
scores_df
{% endhighlight %}




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: left;
    }
</style>
<table border="0" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>nnet_mse_test</th>
      <th>nnet_r2_test</th>
      <th>rf_mse_test</th>
      <th>rf_r2_test</th>
      <th>svr_mse_test</th>
      <th>svr_r2_test</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>ECFP4</th>
      <td>1.378013</td>
      <td>0.045576</td>
      <td>1.216157</td>
      <td>0.157679</td>
      <td>1.359439</td>
      <td>0.058440</td>
    </tr>
    <tr>
      <th>ECFP6</th>
      <td>1.238698</td>
      <td>0.142066</td>
      <td>1.182595</td>
      <td>0.180924</td>
      <td>1.340282</td>
      <td>0.071709</td>
    </tr>
    <tr>
      <th>RDKFP</th>
      <td>1.236841</td>
      <td>0.143353</td>
      <td>1.068570</td>
      <td>0.259899</td>
      <td>1.069886</td>
      <td>0.258988</td>
    </tr>
    <tr>
      <th>TPATF</th>
      <td>3.357452</td>
      <td>-1.325401</td>
      <td>0.704787</td>
      <td>0.511858</td>
      <td>0.970373</td>
      <td>0.327911</td>
    </tr>
    <tr>
      <th>TPAPF</th>
      <td>1.391893</td>
      <td>0.035962</td>
      <td>0.829020</td>
      <td>0.425813</td>
      <td>0.830663</td>
      <td>0.424675</td>
    </tr>
  </tbody>
</table>
</div>


 
Overall, the TPATF fingerprint performed the best — even outperforming the
simple descriptor model. The default random forest had the best performance out
of all the regression methods, although it is very possible that this
will change after some optimization of the model parameters. 
 
In later works, we will further tune models using simple physical descriptors as
well as TPATF fingerprints, and compare their performance to existing logP
predictors using this dataset, as well as the PHYSPROP set. It would also be
interesting to observe the effects of consensus scoring using several models. 
